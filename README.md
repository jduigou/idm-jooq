# Homework SQL : IDM  #

### Exercise Order ###

> * We consider SQL. The exercice is to develop three examples of SQL queries in (1) plain-text language (2) with a corresponding internal
« shape »
_–  3 * 2 = 6_
* Source code and instructions on how to execute the SQL queries


### Project in Java with an internal DSL Jooq ###

The maven project contains :
1. a database : book.db ;
2.  an App class in which queries are executed.

### How to run the project ###

Use jar file to run console based program :

``` java -jar idm.jar ```