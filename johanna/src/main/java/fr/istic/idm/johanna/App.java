package fr.istic.idm.johanna;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 * This program shows how perform queries to a SQLite database with the internal Jooq DSL 
 * @author Johanna Duigou  M2 MIAGE 
 */
public class App 
{
	public static Connection connectionDB = null;
	public static DSLContext database;

	
	/**
	 * Entry of running this program
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		connectionDB = connect();

		// initialize DSLContext
		database = DSL.using(connectionDB, SQLDialect.SQLITE);

		// fetch generated record types
		Result<Record> result = database.select().from("Book").fetch();

		//Query 1 has to display the 4 books : Jeu de Go, Tsumego, Joseki &  Le kô sous toutes ses formes
		showQ1(result);

		//fetch book with title "Jeu de Go"
		result = database.select().from("Book").where("title = 'Jeu de Go'").fetch();

		//Query 2 has to display the book : Jeu de Go
		showQ2(result);

		//fetch all books except the books with ISBN13 = 1234567891234
		result = database.select().from("Book").where("ISBN13 != '1234567891234'").fetch();

		//Query 3 has to display the books : Tsumego, Joseki & Le kô sous toutes ses formes
		showQ3(result);

		//close the connection to database
		connectionDB.close();

	}
	
	/**
	 * Display results Q1
	 * @param result : Result of Q1
	 */
	private static void showQ1(Result<Record> result) 
	{

		for (Record r : result) 
		{
			Integer myRating = (Integer) r.getValue("MyRating");
			String title  = (String) r.getValue("Title");

			System.out.println("I give a grade " + myRating + "/5 to book : " + title);
		}
	}

	/**
	 *  Display results Q2
	 * @param result: Result of Q2
	 */
	private static void showQ2(Result<Record> result) 
	{

		for (Record r : result) 
		{
			String title  = (String) r.getValue("Title");

			System.out.println("The book " + title + " is recorded in the database !");
		}
	}

	/**
	 *  Display results Q3
	 * @param result : : Result of Q3
	 */
	private static void showQ3(Result<Record> result) 
	{

		for (Record r : result) 
		{
			String title  = (String) r.getValue("Title");
			String isbn13  = (String) r.getValue("ISBN13");

			System.out.println("The book " + title + " doesn't have the correct ISBN : " + isbn13 );
		}
	}

	/**
	 * Make JDBC connection to a SQLite database : book.db
	 * @return Connection connectionDB
	 */
	public static Connection connect()
	{

		try 
		{
			Class.forName("org.sqlite.JDBC");
			connectionDB = DriverManager.getConnection("jdbc:sqlite:book.db");

			System.out.println("Connection to book.db with success");
		} 
		catch (ClassNotFoundException notFoundException) 
		{
			notFoundException.printStackTrace();
			System.out.println("Connection error");
		} 
		catch (SQLException sqlException) 
		{
			sqlException.printStackTrace();
			System.out.println("Connection error");
		}
		return connectionDB;
	}

	/**
	 * Close JDBC connection to a SQLite database : book.db
	 */
	public static void close(Connection connectionDB) 
	{
		try 
		{
			connectionDB.close();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
}
